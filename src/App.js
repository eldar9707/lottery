import './App.css';
import Number from "./Components/Number";
import {Component} from "react";
import { AppBar, Box, Button, Container, Toolbar, Typography } from "@material-ui/core";


class App extends Component {
    state = {
        numbers: [
            {id: 0, number: 0},
            {id: 0, number: 0},
            {id: 0, number: 0},
            {id: 0, number: 0},
            {id: 0, number: 0}
        ]
    };

    setNumber = () => {
        let arr = []
        while(arr.length < 5){
            let randNumber = Math.floor(Math.random() * (36 - 5 + 1)) + 5;
            if(arr.indexOf(randNumber) > -1) continue;
            arr[arr.length] = randNumber;
        }
        const numbers = this.state.numbers.map((value, index) => {
            return {
               number: arr[index]
            };
        }).sort(function(a, b) {
            return a.number - b.number;
        });
        this.setState({numbers});
    };

    render() {
        return (
            <div className="App">
                <AppBar position="fixed">
                    <Container fixed>
                        <Toolbar>
                            <Typography  variant="h3">Lottery</Typography>
                        </Toolbar>
                    </Container>
                </AppBar>

                <Box mt={11}>
                    <Box>
                        <Button size="large" color="primary" variant="contained" onClick={this.setNumber}>New Numbers</Button>
                    </Box>
                    {
                        this.state.numbers.map((number, index) =>
                            <Number key={index} number={number.number} />
                        )
                    }
                </Box>
            </div>
        );
    };
}

export default App;
