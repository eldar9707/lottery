import React from "react";

const Number = props => {
    return (
        <div className='number'>
            <h1>{props.number}</h1>
        </div>
    );
};

export default Number;